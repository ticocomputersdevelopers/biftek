<section id="main-content"> 

	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')
	
	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Grad/Mesto') }}</h3>
				<div class="manufacturer"> 
					<select class="JSretailStore search_select">
						<option value="{{ AdminOptions::base_url() }}admin/prodajni_objekti/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach($mesto as $row)
						<option value="{{ AdminOptions::base_url() }}admin/prodajni_objekti/{{ $row->mesto_id }}" @if($row->mesto_id == $mesto_id) {{ 'selected' }} @endif>({{ $row->mesto_id }}) {{ $row->mesto }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
	<section class="medium-5 columns">
			<div class="flat-box">
				<h1 class="title-med">{{ $title }}</h1>
			
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/prodajni_objekti_mesto-edit/{{ $mesto_id }}" enctype="multipart/form-data">
					<input type="hidden" name="mesto_id" value="{{ $mesto_id }}">

					<div class="row">
						<div class="columns {{ $errors->first('mesto') ? ' error' : '' }}">
							<label for="mesto">{{ AdminLanguage::transAdmin('Naziv mesta/grada') }}</label>
							<input type="text" name="mesto" value="{{ Input::old('mesto') ? Input::old('mesto') : $mesto_naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					<div class="row">
						<div class="columns medium-4 {{ $errors->first('ptt') ? ' error' : '' }}">
							<label for="ptt">{{ AdminLanguage::transAdmin('PTT') }}</label>
							<input type="text" name="ptt" value="{{ Input::old('ptt') ? Input::old('ptt') : $ptt }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div> 
					@endif

				</form>
				<div class="add-new-shop-place-button inline-block">
					<a href="{{ AdminOptions::base_url() }}admin/prodajni_objekti/{{ $mesto_id }}/{{$prodajni_objekti_id}}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Dodaj ulicu') }}"><i class="fa fa-plus-square"></i> {{ AdminLanguage::transAdmin('Dodaj novo mesto') }}</a> 
				</div>
			</div>


			<div class="">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Prodajno mesto') }} 	 
				</h3>
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/prodajni_objekti-save/{{ $prodajni_objekti_id }}" >

					<input type="hidden" name="mesto_id"  value="{{$mesto_id}}">
					<input type="hidden" name="prodajni_objekti_id"  value="{{$prodajni_objekti_id}}">
					

					<div class="">
						<div class="">
							<label>{{ AdminLanguage::transAdmin('Ulica') }}</label>
							<input name="ulica" type="text" value="" autocomplete="off"> 
						</div>
						<div class="">
							<label>{{ AdminLanguage::transAdmin('Broj') }}</label>
							<input name="broj" type="text" value="" autocomplete="off"> 
						</div>
						<div class="">
							<label>{{ AdminLanguage::transAdmin('Telefon') }}</label>
							<input name="telefon" type="text" value="" autocomplete="off"> 
						</div>
					</div>

					<div class=""> 
						<div class="no-padd inline-block relative">
							<input class="checkbox-input" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="flag_mesara" value="1">
							<label class="inline-block checkbox-if-shop">
								<!-- <span class="checkmark"></span> -->
								{{ AdminLanguage::transAdmin('Mesara') }}
							</label>
						</div>

						<div class="pull-right add-new-shop-place-button inline-block"> 
							<button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}">{{ AdminLanguage::transAdmin('Potvrdi') }}</button>
							
						</div>
					</div> 

				</form>	

		</section>
		<section class="medium-4 columns shop-info-section">
				@if(count($prodajni_objekti) > 0)
					@foreach($prodajni_objekti as $row)
			<div class="shop-information">
						<div class="shop-type">
							@if($row->flag_mesara == 1)
							<span>{{ AdminLanguage::transAdmin('Mesara') }}</span>
							@else
							<span>{{ AdminLanguage::transAdmin('Ćevabdžinica') }}</span>
							@endif		
						</div>
						<div>			
							<span>{{ AdminLanguage::transAdmin('Ulica') }}:</span>
							<span>{{ $row->ulica }} </span>	
						</div>	
						<div>		
							<span>{{ AdminLanguage::transAdmin('Broj') }}:</span>
							<span>{{ $row->broj }} </span>		
						</div>			
						<div>
							<span>{{ AdminLanguage::transAdmin('Telefon') }}:</span>
							<span>{{ $row->telefon }} </span>
						</div>
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/prodajni_objekti-delete/{{ $row->prodajni_objekti_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
				
					
			</div>
					@endforeach
				@endif
		</section>
	

		
	</div> 
</section>