
<div class="row"> 
	<div class="napomena">
		<p>{{ AdminLanguage::transAdmin('Kod plaćanja - poziv na broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>

	@if(DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id') == 1)
		<p><strong>Prosečno vreme isporuke je do 90 minuta.</strong></p>
		@endif
<!-- 		@if(AdminOptions::web_options(311)==0)
		<p><strong>Napomena o poreskom oslobađanju: "{{AdminOptions::company_name()}}" nije u sistemu pdv-a </strong></p>
		@else
		<p><strong>Napomena o poreskom oslobađanju: Nema </strong></p>
		@endif -->
		<p>{{substr(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena'),0,220)}}</p>
	</div>
</div>

<div class="row"> 
	<table class="signature">
		<tr>  
			<td>&nbsp;</td>
			<td class="text-right">
				<span class="robu_izdao" style=" vertical-align: middle;">{{ AdminLanguage::transAdmin('Račun izdao') }}</span> 
				<img style="max-width: 220px; max-height: 90px;" src="{{ AdminSupport::potpis() }}">
			</td>
		</tr>

		<tr>
			<td style='width: 60%;'>
				<p style="border-top: 1px solid #666; font-size:12px;"> {{ AdminLanguage::transAdmin('Račun je izrađen na računaru i punovažan je bez pečata i potipisa') }}</p>
			</td> 
		</tr>

		<tr>
			<td>&nbsp;</td>
		</tr>

		<tr>
			<td style='width: 70%;'>
				<p style="font-size:12px;"> {{ AdminLanguage::transAdmin('*Zbog prirode robe moguće su minimalne oscilacije u težini proizvoda.') }}</p>
			</td> 
		</tr>
	</table>
</div>