<!DOCTYPE html>

<html>

<head>

    <title>{{ B2bOptions::company_name()}} Admin</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{{ B2bOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ B2bOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />

</head>

<body class="body-login">
    <div class="color-overlay-image"></div>
    <div class="color-overlay"></div>
<!-- LOGIN FORM -->

<div class="login-form-wrapper">

    <section class="login-form b2b-login">

        <a class="logo logo-b2b-login" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}"><img class="b2b-login-img" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt=""></a>
        <span class="logo-text">B2B</span>
        <form action="{{route('b2b.login.store')}}" method="post" >

            <div class="field-group">
                <label for="user-name">Korisnik</label>
                <input id="username" name="username" type="text" class="login-form__input">
                @if($errors->first('username'))
                    <span class="error">{{ $errors->first('username') }}</span>
                @endif
            </div>
                
            <div class="field-group">
                <label for="password">Lozinka</label>
                <input id="password" name="password" type="password" class="login-form__input">

                @if($errors->first('password'))
                   <span class="error">{{ $errors->first('password') }}</span>
                @endif
            </div>
            
            <div class="btn-container center">
                <button class="submit admin-login btn btn-primary btn-round btn-large" onclick="login()">Prijavi se</button>
            </div>
            @if(!B2bOptions::info_sys('wings') AND !B2bOptions::info_sys('calculus'))
            <a class="btn btn-secondary btn-round btn-small forget-password" href="{{ route('b2b.forgot_password') }}">Zaboravljena lozinka</a>
            @endif
            @if(!B2bOptions::info_sys('calculus'))
			<a class="submit btn btn-secondary btn-round btn-small became-partner" href="{{route('b2b.registration')}}">Postanite partner</a>
            @endif

        </form>


    </section>

</div>




</body>

</html>