@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div id="JSeducation_form"> 
	<div class="row">
		<div class="col-xs-12 no-padding">
			<div class="zaposlenje">
				<h2>Zaposlenje u Bifteku</h2>
				<h4 class="my-3 pl-1">Da li želite da postanete član našeg tima?</h4>

				<p class="pl-1">Industrija mesa Biftek je u stalnoj potrazi za adekvatno obrazovanim, ambicioznim i sposobnim ljudima koji su željni napredovanja i usavršavanja. <br>
					Sve što je potrebno jeste da popunite formu, izaberete radno mesto za koje ste zainteresovani i priložize CV.
				</p>
			</div>
		</div>
	</div>
	<h2> <span class="heading-background"> {{ Language::trans('Prijava na konkurs IM Biftek') }} </span> </h2>

	<form method="POST" action="/posao-edit" enctype="multipart/form-data">

	<div class="row">	
			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Ime') }} *</label>
					<input type="text" name="ime" value="{{ Session::get('ime') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'ime')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Prezime') }} *</label>
					<input type="text" name="prezime" value="{{ Session::get('prezime') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'prezime')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Datum rođenja') }} *</label>
					<input type="text" name="datum_rodj" value="{{ Session::get('datum_rodj') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'datum_rodj')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Adresa i mesto stanovanja') }} *</label>
					<input type="text" name="adresa" value="{{ Session::get('adresa') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'adresa')}}</div>
					@endif
				</div>
			</div>



		<h4> <span class="heading-background"> {{ Language::trans('Radno iskustvo') }} </span> </h4>
		<div>
			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Naziv firme') }} </label>
					<input type="text" name="naziv_firme" value="{{ Session::get('naziv_firme') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'naziv_firme')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Period rada') }} </label>
					<input type="text" name="period_rada" value="{{ Session::get('period_rada') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'period_rada')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Pozicija') }} </label>
					<input type="text" name="pozicija_stara" value="{{ Session::get('pozicija_stara') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'pozicija_stara')}}</div>
					@endif
				</div>
			</div>



			<div class="col-xs-12 no-padding">
				<input type="button" value="Dodaj radno iskustvo" id="hideshow" onclick="return change(this);" />
			</div>
		<div id="content" style="display:none">
			<div class="col-xs-12">
				<div class="form-group">
					<label>{{ Language::trans('Naziv firme') }} </label>
					<input type="text" name="naziv_firme2" value="{{ Session::get('naziv_firme2') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'naziv_firme2')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Period rada') }} </label>
					<input type="text" name="period_rada2" value="{{ Session::get('period_rada2') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'period_rada2')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Pozicija') }} </label>
					<input type="text" name="pozicija_stara2" value="{{ Session::get('pozicija_stara2') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'pozicija_stara2')}}</div>
					@endif
				</div>
			</div>



			<div class="col-xs-12 no-padding">	
				<input type="button" value="Dodaj radno iskustvo" id="hideshow2" onclick="return change(this);" />
			</div>

		<div id="content2" style="display:none">
			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Naziv firme') }} </label>
					<input type="text" name="naziv_firme3" value="{{ Session::get('naziv_firme3') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'naziv_firme3')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Period rada') }} </label>
					<input type="text" name="period_rada3" value="{{ Session::get('period_rada3') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'period_rada3')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Pozicija') }} </label>
					<input type="text" name="pozicija_stara3" value="{{ Session::get('pozicija_stara3') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'pozicija_stara3')}}</div>
					@endif
				</div>
			</div>

		</div>
		</div>

	</div>


			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group experience_part">
					<label>{{ Language::trans('Pozicija za koju konkurišete') }} *</label> <br>

					<label> <input type="radio" value="Mesar" {{ Session::get('pozicija') == 'mesar' ? 'checked' : '' }} name="pozicija"> {{ Language::trans('Mesar') }} </label>

					<label> <input type="radio" value="Kasir/Prodavac" {{ Session::get('pozicija') == 'kasir_prodavac' ? 'checked' : '' }} name="pozicija"> {{ Language::trans('Kasir/Prodavac') }} </label>

					<label><input type="radio" value="Roštilj majstor" {{ Session::get('pozicija') == 'roštilj_majstor' ? 'checked' : '' }} name="pozicija"> {{ Language::trans('Roštilj majstor') }} </label>
					<label><input type="radio" id="drugo" value="drugo" {{ Session::get('pozicija') == 'drugo' ? 'checked' : '' }} name="pozicija"> {{ Language::trans('Drugo') }} </label><br>
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'pozicija')}}</div>
					@endif

					<div id="optional">

						<label>{{ Language::trans('Unesite drugu poziciju') }} *</label>
						<input type="text" name="pozicija_text" value="{{ Session::get('pozicija_text') }}">
						@if(Session::has('error_message'))
						<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'pozicija_text')}}</div>
						@endif

					</div>
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Kontakt telefon') }} *</label>
					<input type="text" name="broj_telefona" value="{{ Session::get('broj_telefona') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'broj_telefona')}}</div>
					@endif
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<div class="form-group">
					<label>{{ Language::trans('Email adresa') }} </label>
					<input type="text" name="email" value="{{ Session::get('email') }}">
					@if(Session::has('error_message'))
					<div class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'email')}}</div>
					@endif
				</div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12 text-right-cv no-padding">
				<div class="form-group">
					<label> {{ Language::trans('Vaša biografija (CV)* (PDF format, do 10 mb)') }}: </label>
					<input type="file" name="import_file" accept="application/pdf" class="uploadBtn">
					@if(Session::has('error_message'))
					<div id="JSimport_file_error" class="error red-dot-error">{{MailController::list_message(Session::get('error_message'),'import_file')}}</div>
					@endif
				
				</div>
			</div>

			<div class="col-xs-12 no-padding">
				<input class="btn-job" type="submit" value="Pošalji">
			</div>

		</div>
	</form>


	<script type="text/javascript">
		$(document).ready(function(){
			$('input[name="import_file"]').bind('change', function() {
        	if((this.files[0].size/1024/1024) > 10){
        		$(this).val('');
        		$('#JSimport_file_error').text("{{ Language::trans('Veličina fajla je neodgovarajuća!') }}");
        	}else{
        		$('#JSimport_file_error').text("");
        	}
        	});

			$('input[type="radio"]').click(function(e) {
  			if(e.target.value === 'drugo') {
    			$('#optional').show();
  			} else {
    			$('#optional').hide();
  			}
			})

			if (document.getElementById('drugo').checked) {
				$('#optional').show();
			}else {
				$('#optional').hide();
			}



			jQuery(document).ready(function(){
    				jQuery('#hideshow').on('click', function(event) {        
        			jQuery('#content').toggle('hidden');
    			});
			});

			jQuery(document).ready(function(){
    				jQuery('#hideshow2').on('click', function(event) {        
        			jQuery('#content2').toggle('hidden');
    			});
			});

			jQuery(document).ready(function(){
    				jQuery('#hideshow3').on('click', function(event) {        
        			jQuery('#content3').toggle('hidden');
    			});
			});

			@if(Session::has('message'))
            alertSuccess("{{ Session::get('message') }}")
        	@endif
		});

		function change( el )
		{
    		if ( el.value === "Dodaj radno iskustvo" )
        		el.value = "Obriši radno iskustvo";
    		else
        		el.value = "Dodaj radno iskustvo";
		}
	</script>
@endsection