@extends('shop/themes/'.Support::theme_path().'templates/main')


@section('page') 

<div class="JSloader text-center hidden-sm hidden-xs">
	<div class="load-ring inline-block">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>

@foreach($sekcije as $sekcija)
	<div {{ !empty($sekcija->boja_pozadine) ? 'style="background-color:'.($sekcija->boja_pozadine).'"' : (($sekcija->puna_sirina == 1) ? 'style="background-color: #fff"' : '') }};">  

	@if($sekcija->tip_naziv == 'Text')
	<!-- TEXT SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }} JSfade-in"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">

				{{ Support::pageSectionContent($sekcija->sekcija_stranice_id) }}

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Lista artikala' AND !is_null($sekcija->tip_artikla_id))
	<!-- PRODUCTS SECTION -->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }} JSfade-in"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">

				@if($sekcija->tip_artikla_id == -1 AND count($latestAdded = Articles::latestAdded(5)) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSproducts_slick"> 
					@foreach($latestAdded as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif($sekcija->tip_artikla_id == -2 AND count($mostPopularArticles = Articles::mostPopularArticles(5)) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSproducts_slick"> 
					@foreach($mostPopularArticles as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach 
				</div> 

				@elseif($sekcija->tip_artikla_id == -3 AND count($bestSeller = Articles::bestSeller()) > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSproducts_slick"> 
					@foreach($bestSeller as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif($sekcija->tip_artikla_id == 0 AND All::broj_akcije() > 0)

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div class="JSproducts_slick"> 
					@foreach(Articles::akcija(null,20) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div> 

				@elseif(All::provera_tipa($sekcija->tip_artikla_id)) 

				<h2><span class="section-title JSInlineShort" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2> 

				<div class="JSproducts_slick"> 
					@foreach(Articles::artikli_tip($sekcija->tip_artikla_id,20) as $row)
					@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
					@endforeach
				</div>   

				@endif

			</div>
		</div>
	</div>


	@elseif($sekcija->tip_naziv == 'Lista vesti')
	<!-- BLOGS SECTION-->
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }} JSfade-in" id="cursive"> 
		<div class="row padding-v-20">
			<div class="col-xs-12">

				<h2 class="cursiveBlog"><span class="section-title JSInlineShort cursiveBlog" data-target='{"action":"page_section","id":"{{ $sekcija->sekcija_stranice_id }}"}'>{{Language::trans($sekcija->naziv)}}</span></h2>

				<div>   
					@foreach(All::getShortListNews() as $row) 
					<div class="col-md-4">
						<div class="card-blog"> 
							@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))

							<a class="bg-img center-block" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></a>

							@else

							<iframe src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

							@endif

							<h3 class="blogs-title">
								<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
							</h3>

							<div class="blogs-date text-uppercase">
								{{ Support::date_convert($row->datum) }}
							</div>
						</div>
					</div>
					@endforeach 

				</div> 

			</div>
		</div>
	</div>

	@elseif($sekcija->tip_naziv == 'Slajder' AND !is_null($sekcija->slajder_id))

	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }} JSfade-in"> 

		@if($slajder = Slider::slajder($sekcija->slajder_id) AND count($slajderStavke = Slider::slajderStavke($slajder->slajder_id)) > 0)

			@if($slajder->slajder_tip_naziv == 'Slajder')

			<div class="JSmain-slider">

				@foreach($slajderStavke as $slajderStavka)
				
				<div class="relative">

					<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

					<div class="bg-img" style="background-image: url( '{{ Options::domain() }}{{ $slajderStavka->image_path }}' )"></div>
					<!-- <div class="bg-img-custom">
						<img src='{{ Options::domain() }}{{ $slajderStavka->image_path }}' alt="">
					</div> -->


					<div class="sliderText text-center"> 

						@if($slajderStavka->naslov != '') 
						<h2 class="main-desc JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
						@endif
					</div>
				</div>
				@endforeach
			</div>

			@elseif($slajder->slajder_tip_naziv == 'Baner')
			<!-- BANNERS SECTION -->
				@foreach($slajderStavke as $slajderStavka)
				<div class="relative banners"> 

					<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

					<div class="bg-img" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">

						<div class="banner-desc text-center">

							@if($slajderStavka->naslov != '') 
							<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->naslov }}
							</h2> 
							@endif

							@if($slajderStavka->sadrzaj != '') 
							<div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->sadrzaj }}
							</div> 
							@endif

							@if($slajderStavka->naslov_dugme != '') 
							<a href="{{ $slajderStavka->link }}" class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}</a> 
							@endif
						</div> 
					</div> 
				</div>
				@endforeach			

			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst levo)')
			<!-- TEXT BANNER LEFT SECTION -->
		<div class="row" id="backdrop">
			<div class="backdrop">
				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>

				<div class="col-xs-12"><img src="images/meat/1.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/2.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/3.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/4.png" alt=""></div>
				<div class="col-xs-12"><img src="images/meat/5.png" alt=""></div>
			</div>
			
			@foreach($slajderStavke as $slajderStavka) 
				<div class="col-md-2 col-sm-6 col-xs-12 mainFlip">
				<div class="flip-card">
					<div class="flip-card-inner">
						<div class="flip-card-front">
						<img src="{{ Options::domain() }}{{ $slajderStavka->image_path }}">
						<div class="bottom">
							<h2>{{ $slajderStavka->naslov }}</h2>
							<span>{{ $slajderStavka->sadrzaj }}</span>
						</div>
						</div>
						<div class="flip-card-back">
						<img src="images/juicy.jpg" alt="">
						</div>
					</div>
				</div>
				</div>	
			@endforeach
		</div>


			@elseif($slajder->slajder_tip_naziv == 'Tekst Baner (tekst desno)')
			<!-- TEXT BANNER RIGHT SECTION -->
		<div class="flexing-banner row flex">
				<div class="container">
					@foreach($slajderStavke as $slajderStavka) 
					<div class="col-md-3 col-sm-6 col-xs-12 border-bottom">

						<div class="row padding-v-20 flex" id="banner_right">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 txt-banner xs-no-padding">
								<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
								<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
							</div>

							<div class="col-lg-8 col-md-12 col-sm-12 col-xs-9">
								<div>   
									@if($slajderStavka->naslov != '') 
									<h2 class="my-3" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
										{{ $slajderStavka->naslov }}
									</h2> 
									
									@endif

									@if($slajderStavka->sadrzaj != '') 
									<br>
									<div class="content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
										{{ $slajderStavka->sadrzaj }}
									</div> 
									@endif

									@if($slajderStavka->naslov_dugme != '') 
									<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
										{{ $slajderStavka->naslov_dugme }}
									</a> 
									@endif
								</div>
							</div>
						</div>
					</div>
					@endforeach
			</div> 
			<!-- END OF CONTAINER -->

			<!-- @foreach($slajderStavke as $slajderStavka) 
			<div class="row padding-v-20 flex">
				<div class="col-md-5 col-sm-5 col-xs-12 no-padding txt-banner">
					<a href="{{ $slajderStavka->link }}" class="slider-link"></a>
					<div class="bg-img" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"></div>
				</div>

				<div class="col-md-7 col-sm-7 col-xs-12 text-center">
					<div>   
						@if($slajderStavka->naslov != '') 
						<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
							{{ $slajderStavka->naslov }}
						</h2> 
						@endif

						@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
						@endif

						@if($slajderStavka->naslov_dugme != '') 
						<a href="{{ $slajderStavka->link }}" class="slider-btn-link JSInlineShort inline-block" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->naslov_dugme }}
						</a> 
						@endif
					</div>
				</div>
			</div>
			@endforeach -->
			@elseif($slajder->slajder_tip_naziv == 'Galerija Baner')
			<!-- GALLERY BANNER SECTION -->
			<!-- <div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="gallery-ban">
						@if(isset($slajderStavke[0]))  
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavke[0]->image_path }}' )">
							<a href="{{ $slajderStavke[0]->link }}" class="slider-link"></a> 

							<h2 class="gallery-title"> 
								@if($slajderStavke[0]->naslov != '')
								{{ $slajderStavke[0]->naslov }}
								@endif
							</h2> 
						</div>
						@endif

						@foreach(array_slice($slajderStavke,1) as $position => $slajderStavka) 

						@if($position == 0 || $position == 3)   
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )"> 
							<a href="{{ $slajderStavka->link }}" class="slider-link"></a> 
							<h2 class="gallery-title"> 
								@if($slajderStavka->naslov != '')
								{{ $slajderStavka->naslov }}
								@endif
							</h2> 
						</div> 
						@endif

						@if($position == 1 || $position == 2)   
						<div class="bg-img relative" style="background-image: url( '/{{ $slajderStavka->image_path }}' )">
							<a href="{{ $slajderStavka->link }}" class="slider-link"></a>

							<h2 class="gallery-title"> 
								@if($slajderStavka->naslov != '')
								{{ $slajderStavka->naslov }}
								@endif
							</h2>  
						</div>
						@endif
						@endforeach 
					</div> 
				</div>
			</div> -->
			<div class="row padding-v-20">  
				<div class="col-xs-12"> 
					<div class="row bannerSlider">
						@foreach($slajderStavke as $slajderStavka)
					
						<div class="col-xs-3 my-1">
							@if($slajderStavka->naslov != '') 
								<h2 class="olive" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'> 
									{{ $slajderStavka->naslov }}
								</h2> 
							@endif
							
			
							<!-- @if($slajderStavka->sadrzaj != '') 
							<div class="short-desc JSInlineFull content my-3" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
								{{ $slajderStavka->sadrzaj }}
							</div> 
							@endif -->

							<div class="heightImageLimit">
								<a class="center-block" href="{{ $slajderStavka->link }}">
									<img class="img-responsive margin-auto" src="{{ Options::domain() }}{{ $slajderStavka->image_path }}" alt="{{$slajderStavka->alt}}" />
								</a>
							</div>
						</div>
						@endforeach
					</div> 
				</div>
			</div>

			
				<div class="bannerDescSlick">
					@foreach($slajderStavke as $slajderStavka)
					@if($slajderStavka->sadrzaj != '') 
						<div class="short-desc JSInlineFull content my-3" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
							{{ $slajderStavka->sadrzaj }}
						</div> 
					@endif
					@endforeach
				</div>
		
		

			@elseif($slajder->slajder_tip_naziv == 'Galerija Slajder')
			<!-- GALLERY SLIDER SECTION -->
			<div class="biftek-gallery">
				<div class="main-brand">
					<div class="link">
						<ul class="desktopLinks">
							<li>Home</li>
							<li>services</li>
							<li>testimonials</li>
						</ul>
						<ul class="mobileDots">
							<li><span></span></li>
							<li><span></span></li>
							<li><span></span></li>
						</ul>
					</div>
				</div>

				<div class="main-body">
					<div class="header">
					@foreach($slajderStavke as $slajderStavka) 
						<img src="{{ Options::domain().$slajderStavka->image_path }}" alt="">
					@endforeach
					</div>
				</div>
			</div>
			@endif 

		@endif

	</div>  <!-- CONTAINER -->

	@elseif($sekcija->tip_naziv == 'Newsletter' AND Options::newsletter()==1 AND !is_null($newslatter_description = All::newslatter_description()))
	 
	<div class="{{ ($sekcija->puna_sirina == 1) ? 'container-fluid no-padding' : 'container' }} JSfade-in">
		<div class="row padding-v-20 newsletter"> 

			<div class="col-md-6 col-sm-6 col-xs-12"> 
				<h5 class="JSInlineShort" data-target='{"action":"newslatter_label"}'>
					{{ $newslatter_description->naslov }}
				</h5> 

				<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
					{{ $newslatter_description->sadrzaj }}
				</p>
			</div> 


			<div class="col-md-6 col-sm-6 col-xs-12">          
				<div class="relative"> 
					<input type="text" placeholder="E-mail" id="newsletter" /> 
					<button type="button" class="text-uppercase button" onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>

		</div> 
	</div>

	@endif

	</div> <!-- END BACKGROUND COLOR -->
@endforeach   


@if(isset($anketa_id) AND $anketa_id>0)
@include('shop/themes/'.Support::theme_path().'partials/poll')		
@endif

@if(Session::has('pollTextError'))
<script>
	$(document).ready(function(){     
		bootboxDialog({ message: "<p>{{ Session::get('pollTextError') }}</p>" }); 
	});
</script>
@endif

@endsection
