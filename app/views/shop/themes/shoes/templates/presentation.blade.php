<!DOCTYPE html>
<html lang="sr">

    <head>
        @include('shop/themes/'.Support::theme_path().'partials/head')
         
        <script type="application/ld+json">
        @if($strana == All::get_page_start())
            { 
                "@context" : "http://schema.org",
                "@type" : "WebSite",
                "name" : "<?php echo Options::company_name(); ?>",
                "alternateName" : "<?php echo $title; ?>",
                "url" : "<?php echo Options::domain(); ?>",
                "publisher": {
                    "@type": "Organization",
                    "name": "<?php echo Options::company_name(); ?>"
                }
            }
        @else
            {
                "@context": "http://schema.org",
                "@type": "WebPage",
                "name": "<?php echo $title; ?>",
                "description": "<?php echo $description; ?>",
                "url" : "<?php echo Options::base_url().$org_strana; ?>",
                "publisher": {
                    "@type": "Organization",
                    "name": "<?php echo Options::company_name(); ?>"
                }
            }
        @endif          
        </script>
    </head>    
       
<body 
    @if($strana == All::get_page_start()) id="presentation-page" @endif 
    @if(Support::getBGimg() != null) style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;"@endif
>
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif
       
        
        @include('shop/themes/'.Support::theme_path().'partials/presentation-header')


        <!-- main.blade -->
        <div class="d-content JSmain relative"> 
         
            @if(isset($custom) AND $custom==1)
                    
                @yield('page')

            @else

                <div class="container">
                    <div class="row">  
                        <div class="col-xs-12">
                            @yield('page')
                        </div>
                    </div>
                </div>

            @endif  
        </div>

        <!-- main.blade end -->


        <!-- FOOTER -->
        @include('shop/themes/'.Support::theme_path().'partials/presentation-footer')
        

    <!-- LOGIN POPUP -->
    
    @include('shop/themes/'.Support::theme_path().'partials/popups')

    <!-- BASE REFACTORING -->
    <input type="hidden" id="base_url" value="{{Options::base_url()}}" />
    <input type="hidden" id="admin_id" value="{{Session::has('b2c_admin'.Options::server()) ? Session::get('b2c_admin'.Options::server()) : ''}}" />
    <input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
 
    <input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />
    
    <!-- js includes -->
    @if(Session::has('b2c_admin'.Options::server()))
    <script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
    <script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
    <script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
    <script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
    @endif
    
    <!-- <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}action_type.js"></script> -->
    <script src="{{Options::domain()}}js/slick.min.js"></script>

    <script src="{{Options::domain()}}js/shop/translator.js"></script>
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>
    <!-- <script src="{{Options::domain()}}js/jquery.fancybox.pack.js"></script> -->
 
    @if($strana=='sve-kategorije')
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}all-categories.js"></script>
    @endif

    @if($strana==Seo::get_korpa())
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}order.js"></script>
    @endif

    <script src="{{Options::domain()}}js/shop/login_registration.js"></script>

    @if($strana=='konfigurator' AND Options::web_options(121))
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}configurator.js"></script>
    @endif

    @if($strana==Seo::get_kontakt())
    <input type="hidden" id="lat" value="{{ All::lat_long()[0] }}" />
    <input type="hidden" id="long" value="{{ All::lat_long()[1] }}" />
    @endif
    
    @if(Options::header_type()==1)
    <script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script> 
    @endif
</body>
</html>
