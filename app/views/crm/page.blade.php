<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no"/> 

        <link rel="icon" type="image/png" href="{{ AdminOptions::base_url()}}favicon.ico">

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.core.css">
        <link rel="stylesheet" href="{{ AdminOptions::base_url()}}css/alertify.default.css">
        @if($strana == 'crm_home' || $strana == 'crm_partneri' || $strana == 'crm_analitika'  || $strana == 'crm_ispis'  || $strana == 'crm_lead' || $strana == 'crm_status' || $strana == 'crm_tip' || $strana == 'crm_logovi' || $strana == 'crm_kontakt_vrsta'  || $strana == 'crm_fakture'  || $strana == 'crm_taskovi' || $strana == 'crm_akcija'  || $strana == 'crm_sifrarnik') 
            <link href="{{ AdminB2BOptions::base_url()}}css/crm.css" rel="stylesheet" type="text/css" />
             <link href="{{ AdminB2BOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />
        @endif
        <script src="{{ AdminOptions::base_url()}}js/alertify.js" type="text/javascript"></script>
  
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122633081-1"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-122633081-1');
        </script>
    </head>
<body class="{{ Session::get('adminHeaderWidth') }}">

  
    <div>            
    
        @include('admin.partials.header')

        @if($strana=='crm_home' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_home')
        @elseif($strana=='crm_ispis' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_ispis')  
        @elseif($strana=='crm_lead' AND Admin_model::check_admin(array('CRM')))
             @include('crm/crm_lead')
        @elseif($strana=='crm_akcija' AND Admin_model::check_admin(array('CRM')))
             @include('crm/crm_akcija')
        @elseif($strana=='crm_sifrarnik' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_sifrarnik')
        @elseif($strana=='crm_status' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_status')
        @elseif($strana=='crm_tip' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_tip')
        @elseif($strana=='crm_partneri' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_partneri')
        @elseif($strana=='crm_kontakt_vrsta' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_kontakt_vrsta')
        @elseif($strana=='crm_analitika' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_analitika')
        @elseif($strana=='crm_fakture' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_fakture')
        @elseif($strana=='crm_taskovi' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_taskovi')
        @elseif($strana=='crm_logovi' AND Admin_model::check_admin(array('CRM')))
            @include('crm/crm_logovi')
        @endif
    </div> <!-- end of flex-wrapper -->

    <!--  articles criteria -->   
    <?php if(isset($criteria)){ ?><script> var criteria = new Array("{{ $criteria['grupa_pr_id'] ? $criteria['grupa_pr_id'] : 0 }}", "{{ isset($criteria['proizvodjac']) ? $criteria['proizvodjac'] : 0 }}", 
    "{{ isset($criteria['dobavljac']) ? $criteria['dobavljac'] : 0 }}", "{{ isset($criteria['tip']) ? $criteria['tip'] : 0 }}", "{{ isset($criteria['labela']) ? $criteria['labela'] : 0 }}","{{ isset($criteria['karakteristika']) ? $criteria['karakteristika'] : 0 }}", "{{ isset($criteria['magacin']) ? $criteria['magacin'] : 0 }}", "{{ isset($criteria['exporti']) ? $criteria['exporti'] : 0 }}","{{ isset($criteria['katalog']) ? $criteria['katalog'] : 0 }}", "{{ isset($criteria['filteri']) ? $criteria['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteria['flag']) ? $criteria['flag'] : 0 }}", "{{ isset($criteria['search']) ? $criteria['search'] : 0 }}", "{{ isset($criteria['nabavna']) ? $criteria['nabavna'] : 'nn-nn' }}"); </script><?php }else{ ?><script> var criteria; </script><?php } ?> 

    <!-- import criteria -->
     <?php if(isset($criteriaImport)){ ?><script> var criteriaImport = new Array("{{ $criteriaImport['grupa_pr_id'] ? $criteriaImport['grupa_pr_id'] : 0 }}", "{{ isset($criteriaImport['proizvodjac']) ? $criteriaImport['proizvodjac'] : 0 }}", 
     "{{ isset($criteriaImport['dobavljac']) ? $criteriaImport['dobavljac'] : 0 }}", "{{ isset($criteriaImport['filteri']) ? $criteriaImport['filteri'] : 'nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn' }}", "{{ isset($criteriaImport['search']) ? $criteriaImport['search'] : 0 }}", "{{ isset($criteriaImport['nabavna']) ? $criteriaImport['nabavna'] : 'nn-nn' }}");</script><?php }else{ ?><script> var criteriaImport; </script><?php } ?>
    <script> var all_ids = {{ isset($all_ids) ? $all_ids : 'null' }}; var fast_insert_option = '{{ AdminOptions:: gnrl_options(3030) }}'; </script>


    <input type="hidden" id="base_url" value="{{AdminOptions::base_url()}}" />
    <input type="hidden" id="live_base_url" value="{{AdminOptions::live_base_url()}}" />
    <script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ AdminOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin.js" type="text/javascript"></script>
    <script src="{{ AdminOptions::base_url()}}js/admin/admin_funkcije.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
    <script src="{{ AdminOptions::base_url()}}js/admin/crm.js" type="text/javascript"></script>
    
    
    <section class="popup info-confirm-popup info-popup">
        <div class="popup-wrapper">
    	    <section class="popup-inner">
    	    </section>
    	</div>
    </section>

</body>
</html>