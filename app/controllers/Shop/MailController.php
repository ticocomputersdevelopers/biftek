<?php 
use Service\Mailer;


class MailController extends Controller {

	public function meil_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

    	if(Options::gnrl_options(3003)){   
    	 		
	    	$data = array(
	    		'name' => Input::get('name'), 
	    		'email' => Input::get('email'),
	    		'msg' => Input::get('message'),
	    		'url' => Options::base_url()
	    	);

	    	Mail::send('shop.email.contact', $data, function($message){
	    	    $message->from(Input::get('email'), Input::get('name'));

	    	    $message->to(Options::company_email())->subject(EmailOption::subject());
	    	});
    	}else{
 		
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('name')." <br />
	        <b>Email:</b> ".Input::get('email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('message')."</p>";
	        $subject="Poruka sa ".parse_url(Options::domain(), PHP_URL_HOST);
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);
    	}

    } 

	public function contact_message_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'contact-name' => 'required|regex:'.Support::regex().'|max:200',
            'contact-email' => 'required|email',
            'contact-message' => 'required|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else {
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('contact-name')." <br />
	        <b>Email:</b> ".Input::get('contact-email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('contact-message')."</p>";
	        $subject="Poruka sa ".Options::server();
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);

	        return Redirect::back()->with('message',Language::trans('Vaša poruka je poslata').'!');
        }
	}

	public function upit() {

		$data = array(
						"strana" => 'posao',
						"title" => 'Posao',
						"description" => '',
						"keywords" => '',
						"org_strana" => ''
					);
		return View::make('shop/themes/'.Support::theme_path().'pages/posao',$data);
	}

	public function posao_edit() {

		$data = Input::get();
		$data ['import_file'] = Input::file('import_file');
		$rules = array(
						'ime' => 'required|regex:'.AdminSupport::regex().'|between:1,50',
						'prezime' => 'required|regex:'.AdminSupport::regex().'|between:1,50',
						'datum_rodj' => 'required|regex:'.AdminSupport::regex().'|between:1,12',
						'adresa' => 'required|regex:'.AdminSupport::regex().'|between:1,70',
						'pozicija' => 'required',
						'broj_telefona' => 'required|between:1,50',
						'email' => 'email|between:1,70',
						'import_file' => '',

						);

		if (Input::get('pozicija') == 'drugo') {
			$rules['pozicija_text'] = 'required';
		}

		$validator_messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Polje je već zauzeto!',
                'max'=>'Veličina fajla je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve!',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!',
                'size' => 'Fajl moze imati najviše 10 mb'
            );
		$validator = Validator::make($data, $rules, $validator_messages);
		if ($validator->fails()) {
			$error_message = $validator->messages()->toArray();
			return Redirect::to('karijera')->with(['error_message' => $error_message,
												'ime' => Input::get('ime'),
												'prezime' => Input::get('prezime'),
												'datum_rodj' => Input::get('datum_rodj'),
												'adresa' => Input::get('adresa'),

												'naziv_firme' => Input::get('naziv_firme'),
												'period_rada' => Input::get('period_rada'),
												'pozicija_stara' => Input::get('pozicija_stara'),

												'naziv_firme2' => Input::get('naziv_firme2'),
												'period_rada2' => Input::get('period_rada2'),
												'pozicija_stara2' => Input::get('pozicija_stara2'),

												'naziv_firme3' => Input::get('naziv_firme3'),
												'period_rada3' => Input::get('period_rada3'),
												'pozicija_stara3' => Input::get('pozicija_stara3'),

												"pozicija" => Input::get('pozicija'),
												"pozicija_text" => Input::get('pozicija_text'),
												"email" => Input::get('email'),
												"broj_telefona" => Input::get('broj_telefona'),
												"import_file" => Input::get('import_file')

												]);
			
		} else {
			$subject = "Konkurs - Industrija mesa Biftek";

			$body = "Ime: ".Input::get('ime')."<br> Prezime: ".Input::get('prezime')."<br> Datum rođenja: ".Input::get('datum_rodj')."<br> Adresa: ".Input::get('adresa')."";

			if(!empty(Input::get('naziv_firme')) || !empty(Input::get('period_rada')) || !empty(Input::get('pozicija_stara'))) {
				$body .= "<h4>Radno iskustvo</h4>Naziv firme: ".Input::get('naziv_firme')."<br> Period rada: ".Input::get('period_rada')."<br> Pozicija: ".Input::get('pozicija_stara')."";

				if(!empty(Input::get('naziv_firme2')) || !empty(Input::get('period_rada2')) || !empty(Input::get('pozicija_stara2'))) {

					$body .= "<h4>Radno iskustvo2</h4>Naziv firme: ".Input::get('naziv_firme2')."<br> Period rada: ".Input::get('period_rada2')."<br> Pozicija: ".Input::get('pozicija_stara2')."";

					if(!empty(Input::get('naziv_firme3')) || !empty(Input::get('period_rada3')) || !empty(Input::get('pozicija_stara3'))) {

						$body .= "<h4>Radno iskustvo3</h4>Naziv firme: ".Input::get('naziv_firme3')."<br> Period rada: ".Input::get('period_rada3')."<br> Pozicija: ".Input::get('pozicija_stara3')."";
					}
				}
			}
			$body .= "<br>";
			if (Input::get('pozicija') == 'drugo') {
				$body .= "<br> Pozicija za koju konkurišem: ".Input::get('pozicija_text')."";
			} else {
				$body .= "<br> Pozicija za koju konkurišem: ".Input::get('pozicija')."";
			}

			if (!empty(Input::get('email'))) {
				$body .= "<br> Email: ".Input::get('email')."";
			}

			$body .= "<br> Broj telefona: ".Input::get('broj_telefona')."";



			$file = Input::file('import_file');
			if (!is_null($file)) {
			
        	$originalName = $file->getClientOriginalName();
        	$extension = $file->getClientOriginalExtension();
			
        	$file->move('files/zaposlenje/', str_replace(" ", "_", $originalName ));

			$attchFile = 'files/zaposlenje/'. str_replace(" ", "_", $originalName );
			} else {
				$attchFile = null;
			}

			Mailer::send(Options::company_email(),Options::company_email(),$subject,$body,$attchFile);

			if(File::exists($attchFile)){
				File::delete($attchFile);
			}

			if (!empty(Input::get('email'))) {
				
			$body2 = "Poštovani,<br>Uspešno ste se prijavili na konkurs Industrije mesa Biftek.<br>O daljem toku konkursa i sledećem krugu selekcije ćemo Vas obavestiti preko kontakt podataka koje ste ostavili.<br> Srdačan pozdrav";

			Mailer::send(Options::company_email(),Input::get('email'),$subject,$body2);

			}


			return Redirect::back()->with("message","Uspešno ste se prijavili za posao!");


		}

	}

	public static function list_message($messages,$pharam)
	{	
		$error_message = null;
		if (isset($messages[$pharam])) {
			foreach ($messages[$pharam] as $message) {
				$error_message .= $message;
				$error_message .= "&nbsp;&nbsp;";

			}
		}
		return $error_message;
	}

}