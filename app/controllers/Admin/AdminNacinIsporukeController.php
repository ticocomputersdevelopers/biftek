<?php

class AdminNacinIsporukeController extends Controller {

	public function index($id) {
		$data = array(
	                "strana"=>'nacin_isporuke',
	                "title"=> 'Način isporuke',	  
	                "nacini_isporuke" => AdminNacinIsporuke::all(),
	                "web_nacin_isporuke_id" => $id,
	                "nacin_isporuke" => AdminNacinIsporuke::getSingle($id)       
	            );
		return View::make('admin/page', $data);
	}

	public function edit($id) {
		$inputs = Input::get();
		
		if(isset($inputs['aktivno'])){
			$aktivno = 1;
		} else {
			$aktivno = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	if($id == 0){
        		$current = DB::select("SELECT MAX(web_nacin_isporuke_id) AS max FROM web_nacin_isporuke")[0]->max;
        		$web_nacin_isporuke_id = $current + 1;
        		DB::table('web_nacin_isporuke')->insert(array('web_nacin_isporuke_id' => $web_nacin_isporuke_id, 'naziv' => $inputs['naziv'], 'selected' => $aktivno));

        		AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_DODAJ', array(DB::table('web_nacin_isporuke')->max('web_nacin_isporuke_id')));

        		return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/0')->withMessage('Uspešno ste sačuvali podatke.');
			} else {
				DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', $id)->update(array('naziv' => $inputs['naziv'], 'selected' => $aktivno));

				AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_IZMENI', array($id));

				return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/'.$id)->withMessage('Uspešno ste sačuvali podatke.');
			}
        }

	}

	public function delete($id) {
		AdminSupport::saveLog('SIFARNIK_NACIN_ISPORUKE_OBRISI', array($id));
		DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id', $id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/nacin_isporuke/0')->withMessage('Uspešno ste obrisali vrednost.');
	}

	public function prodajni_objekti($mesto_id, $prodajni_objekti_id= null) {
		$data = array(
	                "strana"=>'prodajni_objekti',
	                "title"=> 'Prodajni objekti',
	                "mesto_id"=>$mesto_id,
	                "mesto_naziv"=>DB::table('mesto')->where('mesto_id',$mesto_id)->pluck('mesto'),
	                "ptt"=>DB::table('mesto')->where('mesto_id',$mesto_id)->pluck('ptt'),		
	                "mesto"=> DB::table('mesto')->get(),
	                "prodajni_objekti_id"=>$prodajni_objekti_id ? $prodajni_objekti_id : 0,
	                "prodajni_objekti"=>DB::table('prodajni_objekti')->where('mesto_id', $mesto_id)->get()	  
	              
	            );
		return View::make('admin/page', $data);
	}

	public function prodajni_objekti_mesto_edit($id) {
		$inputs = Input::get();
		//All::dd($inputs);
		$validator = Validator::make($inputs, 
			array(
				'mesto' => 'required',
				'ptt'=>'numeric|digits_between:0,10',
				'telefon'=>'numeric|digits_between:0,20'
			));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	$general_data = $inputs;
            if($inputs['mesto_id'] == 0){                
                $general_data['mesto_id'] = DB::select("SELECT MAX(mesto_id) AS max FROM mesto")[0]->max + 1;
            }
          
            if($inputs['mesto_id'] != 0){
                DB::table('mesto')->where('mesto_id',$id)->update($general_data);
            }else{
                DB::table('mesto')->insert($general_data);
               
            }
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/prodajni_objekti/'.$general_data['mesto_id'])->with('message',$message);
        }

	}
	public function prodajni_objekti_save($id) {
		$inputs = Input::get();
		//All::dd($inputs);
		$validator = Validator::make($inputs, 
				array('ulica' => 'required',
					'telefon'=>'numeric|digits_between:1,15'
			));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
        	$general_data = $inputs;

        	if(!isset($general_data['flag_mesara'])){
        		$general_data['flag_mesara']=0;
        	}else{
        		$general_data['flag_mesara']=1;
        	}

            if($inputs['prodajni_objekti_id'] == 0){                
                $general_data['prodajni_objekti_id'] = DB::select("SELECT MAX(prodajni_objekti_id) AS max FROM prodajni_objekti")[0]->max + 1;
            }
          
            if($inputs['prodajni_objekti_id'] != 0){
                DB::table('prodajni_objekti')->where('prodajni_objekti_id',$id)->update($general_data);
            }else{
                DB::table('prodajni_objekti')->insert($general_data);
               
            }
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/prodajni_objekti/'.$general_data['mesto_id'])->with('message',$message);
        }

	}
	public function prodajni_objekti_delete($id) {
		$mesto_id=DB::table('prodajni_objekti')->where('prodajni_objekti_id', $id)->pluck('mesto_id');
		DB::table('prodajni_objekti')->where('prodajni_objekti_id', $id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/prodajni_objekti/'.$mesto_id)->withMessage('Uspešno ste obrisali vrednost.');
	}

	





}