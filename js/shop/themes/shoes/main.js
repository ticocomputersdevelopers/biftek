
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        // $('.JSlazy_load').Lazy();
		$('.JSlazy_load').Lazy({
			scrollDirection: 'vertical',
			visibilityOnly: true,
		});

    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
	});
   
	// Facebook
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '163422577814443',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  }; 

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

// *********************************************************  

// if($('#admin-menu').length) {
// 	$('body').css('margin-top', - $('#admin-menu').height() - $('header').outerHeight());
// } else {
// 	$('body').css('margin-top', - $('header').outerHeight());

// }


if (window.location.href.indexOf("/shop") > -1) {
	$('body').attr('id', 'start-page')
}
if(window.location.href.indexOf('/o-nama') > -1) {
	$('body').attr('id', 'presentation-page');
	$('header').addClass('sticky_head');
	$('header').css('top', 0);
} 

//  
if(!$('body').is('#start-page')) {
	$('.JSCategoriesToAppend').css('display', 'none');
} else {
	$('.JSCategoriesToAppend').css('display', 'none');
}


if($(window).width() > 991) {
	$('.JSCategoriesToAppend').append($('.categoryContent'));
}

if($("body").is('#product-page')) {
	$('.categoryContent').toggleClass('JSCategoriesToAppend');
} else if($('body').is('#artical-page')) {
	$('.categoryContent').toggleClass('JSCategoriesToAppend');
}
// 

// if($('body').is('#product-page')) {
// 	$('.JSclose-category').on('click', function() {
// 		$('.JSCategoriesToAppend').show();
// 		$('.d-content').css('min-height', '450px');
// 		$('body').removeClass('translateBody');
// 	})
// } else if($('body').is('#artical-page')) {
// 	$('.JSclose-category').on('click', function() {
// 		$('.JSCategoriesToAppend').show();
// 		$('.d-content').css('min-height', '450px');
// 		$('body').removeClass('translateBody');
// 	})
// }

// Change image src
// var stickyBackground = '../../../../images/biftek_red.png';

// if($(window).scroll(function() {
// 	var value = $(this).scrollTop();

// 	if(value > 0) {
// 		$('.stickyBg').attr('src', stickyBackground);
// 	} else {
// 		$('.stickyBg').attr('src', stickyBackground);
// 	}
//  }))
 
// End of static navbar
$('.mainWrapper').parent().css('padding', '0');
$('.mainWrapper').parent().parent().removeClass('padding-v-20');
// REMOVE EMPTY LOGGED USER
$('.logged-user').each(function(i, el){ 
	if ( ($(el).text().trim() == '')) {
		$(el).hide();
	}
});


// SLICK SLIDER INCLUDE
if ($('.JSmain-slider').length) {
	if($('#admin_id').val()){

		$('.JSmain-slider').slick({
			autoplay: false,
			draggable: false
		});

	}else{
		$('.JSmain-slider').slick({
			autoplay: true
		});
	} 
}
 
// PRODUCTS SLICK SLIDER 
if ($('.JSproducts_slick').length){

	$('.JSproducts_slick').slick({ 
		autoplay: true,
		slidesToShow: 4,
		slidesToScroll: 1, 
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	}); 
}


// Blog Slick
if ($('.JSBlogSlick')[0]){
	$('.JSBlogSlick').slick({
		autoplay: true,
		slidesToShow: 3,
		arrows: false,
		responsive: [
			{
			  breakpoint: 1160,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1 
			  }
			},
			{
			  breakpoint: 880,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false
			  }
			}
		]
	});
}
// End of blog slick
 
// RELATED ARTICLES TITLE 
	$('.slickTitle').each(function(i, tit){
		if($(tit).next('.JSproducts_slick').find('.slick-track').children('.JSproduct ').length){
			$(tit).show();
		}
	})

	if ($('.JSblog-slick').length){

		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 4,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
				  }
				}
			]
		});
	}
 
	// CATEGORIES - MOBILE  
	
	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length){
			$(li).append('<span class="JSsubcategory-toggler"><i class="fas fa-plus"></i></span>');
		}
	});          

	$('.JSsubcategory-toggler').on('click', function() { 

		if ($(this).siblings('ul').css('display') == 'none') {
			$(this).children().removeClass('fas fa-plus').addClass('fas fa-minus'); 
			$(this).parent().siblings().find('span i').removeClass('fas fa-minus').addClass('fas fa-plus');  
		} else {
			$(this).children().removeClass('fas fa-minus').addClass('fas fa-plus');
		}
 
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp(); 
	});  
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 1;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);
				$('.JSmodal').show();
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});

		$('.JSleft_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]); 
			 
			if (next_prev == img_gallery_img.length - 1) {
				next_prev = 0; 
			} else {
				next_prev++;
			}	  
		}); 

		$('.JSright_btn').on('click', function(){ 
			modal_img.attr('src', img_arr[next_prev]);
	 		next_prev--;

	 		if (next_prev < 0 ) {
	 			next_prev = img_gallery_img.length - 1;
	 		}   
		});  

		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());
 
// ======== SEARCH ============
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("open_cat");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("open_cat");		 
	});
	$('.JSclose-category').on('click', function() {
		// $('.JSCategoriesToAppend').hide();
		// $('.d-content').css('min-height', '450px');
		$('body').removeClass('translateBody');
	})


 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").on('click', function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
  
		if ($(this).children().attr('class') == 'fas fa-plus') {
			$(this).children().removeClass('fas fa-plus').addClass('fas fa-minus'); 
		} else {
			$(this).children().removeClass('fas fa-minus').addClass('fas fa-plus'); 
		} 
	});

	if ($('.selected-filters li').length) { 
		$('.JShidden-if-no-filters').show();
	} 
 
 // SELECT ARROW - FIREFOX FIX
	$('select').wrap('<span class="select-wrapper"></span>');
	 

// POPUP BANER 
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
		setTimeout(function(){ 
			$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		$(document).on('click', function(e){
			var target = $(e.target);
			if(!target.is($('.popup-img'))) { 
				$('.JSfirst-popup').hide();
			} 
		}); 
		if ($('.JSfirst-popup').length) {
			sessionStorage.setItem('popup','1');
		}
	}

	if ($(window).width() > 991 ) {
 
		// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=4) {
				subitems.slice(i, i+4).wrapAll("<div class='clearfix level-2-wrap'></div>");
			}
		});

		// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=4){
	  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
	  	}()); 
 
 	 // SLIDER MARGIN && ANIMATION
	 	// if($('body').is('#start-page')){   
		//  	$('.JSmain-slider').each(function(i, el){
		//  		if ($(el).offset().top < $('header').offset().top + $('header').outerHeight()) {
		//  			$(el).css('margin-top', - $('header').outerHeight());
		//  		} else {
		//  			$(el).css('margin-top', 0);
		//  		}
		//  		$(el).addClass('fadeIn'); 
		//  	}); 
		// }
	}

 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			if(left_link.length) {
				main.before((left_link).css('top', main.position().top + 'px'));
			}
			if(right_link.length) {
				main.after((right_link).css('top', main.position().top + 'px'));
			}
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 

		}, 1000); 
  	} 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt(); 

// OPEN SEARCH MODAL
	$('.JSopen-search-modal').on('click', function(){
		$('.JSsearch-modal').show();
	});
 
	$(document).on('click', function(e){  
		if ( ! $('.search-content *, .JSopen-search-modal, .JSopen-search-modal *').is(e.target)) { 
			$('.JSsearch-modal').hide(); 
		}  
	});
 
 	// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
				return document.cookie
				  .split('; ')
				  .find(row => row.startsWith(cookieParamName))
				  .split('=')[1];
			}else{
				return undefined;
			}
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});

 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   


	// ANIMATE COUNTER WHEN LOADED
	const counters = document.querySelectorAll('.counter');
	const speed = 4000; 
	
	counters.forEach(counter => {
		const updateCount = () => {
			const target = +counter.getAttribute('data-target');
			const count = +counter.innerText;

			const inc = target / speed*70;
	
			if (count < target) {
				counter.innerText = Math.round(count + inc);
				
				setTimeout(updateCount, 1);
			} else {
				counter.innerText = target;
			}
		};
		updateCount();
	});

	if($('body').is('#presentation-page')) {
		$(".categoryContent").hide();
	}

			$('.toggle-btn').on('click', function(e) {
				$('.header img').siblings().removeClass();
			});

			// BIFTEK MAP
				$('.mapHolder').show();
	
					$('.map-fragment img').each(function(i) {
						var $img = $(this);
						setTimeout(function() {
							$img.addClass('animate');
							$img.css('opacity', '1');
						}, i*70); 
					});
			
					$('.marks img').each(function(i) {
						var $img = $(this);
						setTimeout(function() {
							$img.addClass('animate-marks');
							$img.css('opacity', '1');
						}, i*150); 
					});

			   // FADE IN ELEMENT
			   $(window).scroll(function() {
                var windowBottom = $(this).scrollTop() + $(this).innerHeight();
                $(".JSfade-in").each(function() {
                    var objectBottom = $(this).offset().top + 100;

                    if (objectBottom < windowBottom) {
                        if ($(this).css("opacity")==0) {
                            $(this).fadeTo(500, 1);
                        }
                    }
                });
            }).scroll();

			// FADE IN - ABOUT US
			// if($('.card-about-us').parent().parent().parent().is('.JSfade-in')) {
			// 	$(window).scroll(function() {
			// 		$('.card-about-us').addClass('JSfade-about').parent().parent().parent().removeClass('JSfade-in');
			// 		$('.card-about-us-1').addClass('JSfade-about-back');
			// 		$('.card-about-us-2').addClass('JSfade-about-back');
			// 		var windowBottom = $(window).scrollTop() + $(window).innerHeight();
			// 		var objectBottom = $('.card-about-us').offset().top + 300;
			// 		var objectBottomBack = $('.card-about-us-1').offset().top + 200;

			// 		if (objectBottom < windowBottom) {
			// 			if ($('.JSfade-about').css("opacity")==0) {
			// 				$('.JSfade-about').fadeTo(500, 1);
			// 			}
			// 		}
			// 		if (objectBottomBack < windowBottom) {
			// 			if ($('.JSfade-about-back').css("opacity")==0) {
			// 				$('.JSfade-about-back').fadeTo(500, 0.1);
			// 			}
			// 		}
			// 	}).scroll();
			// } 
		
			if($('.JSlevel-1').height() > $('.d-content').height()) {
				if(!$('.JSlevel-1').length) {
					$('.d-content').height($('.JSlevel-1').height())
				}
			}
			if($('body').is('#start-page')) {
				$('.d-content').css('min-height', $(".JSlevel-1").height());
			}
			
			
			// if($(window).width() < 991) {
			// 	$('.JSlevel-1').hide();
			// 	$('.d-content').height('');
			// }
		
			// if($("body").is('#product-page')) {
			// 	$(".categoryOpen").on('click', function(e) {
			// 		$('.JSCategoriesToAppend').toggle().css('top', '0');

			// 	}) 
			// } else if($('body').is('#artical-page')) {
			// 	$(".categoryOpen").on('click', function(e) {
			// 		$('.JSCategoriesToAppend').toggle().css('top', '0');

			// 	}) 
			// }

			$('.categoryOpen').on('click', function(e) {
				$('.JSCategoriesToAppend').toggle();
				$('body').toggleClass('translateBody');
				$('.JSlevel-1').addClass('show-important');
				// $('.categoryContent').show();
				
				// $('.JSStickyLogo').css('z-index', '6');

		
				// if(($('.d-content').height() + 0.01) < $('.JSlevel-1').height()) {
					// $('.d-content').height($('.JSlevel-1').height());
				    // $('.d-content').css('min-height', $(".JSlevel-1").height());

				// }
				// else {
					// $('.d-content').height('');
					// $('.d-content').css('min-height', '450px');
				// }
			});
			

			// $('.categoryContent').css('display', 'none');
			$('.card-about-us').parent().addClass('behindText');

			if($(window).width() < 991) {
				$('.JSRespWrap').prepend($('.icons_header'));
			}

			if(!$('body').is('#presentation-page')) {
				$('.card-blog').css({
					'color': '#000',
					'font-weight': 'bold'
				})
				$('.blogs-title').css('color', '#000');
			}

			if($(window).width() > 991) {
				$('#presentation-page').find('#main-menu').addClass('md-absolute');
			} else if($(window).width() < 991) {
				$('#presentation-page').find('.onStartPage').removeClass('hidden');
			}


}); // DOCUMENT READY END

// ALERTIFY
function alertSuccess(message) {
	swal(message, "", "success");
		
	setTimeout(function() {
		swal.close();  
	}, 2000);
}	
 
function alertError(message) {
	sweetAlert(message, "", "error");
	setTimeout(function() {
		sweetAlert.close();  
	}, 2000);
}

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg').addClass('img-responsive'); 
			}
		});   

		// IMAGE REPLACEMENT
		if($(window).width() > 1024) {
			$('.product-image-wrapper').on('mouseenter', function(){  

				var img = $(this).find('img'),
					placeholder = $(this).find('.JSimg-placeholder');
	 			
	 			if(img.attr('src').includes('no-image')) {
		 
					img.siblings('.JSimg-placeholder').remove();

			  	} else if (img.attr('src') != placeholder.data('src') && placeholder.length) {
 
					sessionStorage.setItem('src', img.attr('src')); 

					img.attr('src', placeholder.data('src')).hide().show();  

			  	} 

			}).on('mouseleave', function(){
				var img = $(this).find('img');
	 
	 			if (sessionStorage.getItem('src')) {
					
					img.attr('src', sessionStorage.getItem('src')).hide().show();

					sessionStorage.removeItem('src');
				}    
			});
			// IF IMAGE CLICKED
			sessionStorage.removeItem('src');
		}
	} 
}